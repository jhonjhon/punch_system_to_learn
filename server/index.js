const express = require('express');
const http = require('http');
const app = express();
const server = http.createServer(app);

const io = require("socket.io")(server, {
    cors: {
        origin: "http://localhost:8080",
        methods: ["GET", "POST"],
        allowedHeaders: ["Access-Control-Allow-Origin"],
        credentials: false
    },
    autoConnect: false,
});

/*
app.get('/', (req, res)=> {
    //res.send('<h1>Hello world</h1>');
    //res.sendFile(__dirname + '/index.html');
});
*/


io.on('connection', (socket) => {

    const users = [];
/*
    for (let [id, socket] of io.of('/').sockets){

        if(socket.username != undefined){
            users.push({
                userID: id,
                username: socket.username
            });
        }

    }

    socket.emit('users', users);
*/

    // notify existing users
    /*
    socket.broadcast.emit("user connected", {
        userID: socket.id,
        username: socket.data.username,
    });
    */

    //Connected
    socket.on('connected', ()=> {
        socket.emit("status", "Connected");
    });

    //Disconnect
    socket.on('disconnect', ()=> {
        socket.emit("status", "Disconnected"); /* NEW */
    });

    //Send to others
    socket.on('message', (data) => {
        socket.broadcast.emit('message', data); // io.emit to all sockets without me
    });

    //typing
    socket.on('typing', (data)=>{
        socket.broadcast.emit('typing', (data))
    });
    //stopTyping
    socket.on('stopTyping', (data)=>{
        socket.broadcast.emit('stopTyping', (data))
    });

    //Joined
    socket.on('joined', (data)=>{

        //console.log('users1', users);

        socket.join('default');
        socket.to('default').emit('joined', (data));

        socket.id = data.id;
        socket.username = data.username;

        for (let [id, socket] of io.of('/').sockets){
            if(socket.username != undefined){
                users.push({
                    userID: id,
                    username: socket.username,
                    connected: true
                });
            }
        }

        //socket.to('default').emit('users', users);
        socket.broadcast.emit('users', users);

        socket.emit("status", "Connected User");

    });


    //Leave
    socket.on('leaved', (data)=>{
        socket.broadcast.emit('leaved', (data));
    });

    socket.on('private message', ({content, to})=>{
        socket.to(to).emit("private message", {
           content,
           from: socket.id
        });
    });

});

server.listen(3000, ()=>{
    console.log('Listening on *:3000');
});
