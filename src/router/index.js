import { createRouter, createWebHashHistory } from 'vue-router'
import CheckIn from '../views/CheckIn.vue'
import Break from '../views/Break.vue'
import Lunch from '../views/Lunch.vue'
import CheckOut from '../views/CheckOut.vue'
import ChatSupport from '../views/ChatSupport.vue'
 
const routes = [
  {
    path: '/',
    name: 'CheckIn',
    component: CheckIn
  },
  {
    path: '/break',
    name: 'Break',
    component: Break
  },
  {
    path: '/lunch',
    name: 'Lunch',
    component: Lunch
  },
  {
    path: '/check-out',
    name: 'CheckOut',
    component: CheckOut
  },
  {
    path: '/chat-support',
    name: 'ChatSupport',
    component: ChatSupport
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
