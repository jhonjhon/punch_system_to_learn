# punch_system_1.0

## Project setup
```
npm install
```
### Change Client-Url
```
https://localhost8080 or https://127.0.0.1:8088 etc...
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
